const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required : [true, "Course Name is Required!"]
	},
	
	description: {
		type: String,
		required: [true, "Desciption is Required!"]	
	},
	price: {
		type: Number,
		required: [true, "Price is Required!"]
	},
	iaActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [{
		userId: {
			type: String,
			required: [true, "UserId is required!"]
		},
		enrolledOn: {
			date: Date,
			default: new Date()
		} 
	}]
})


module.exports = mongoose.model("Course", courseSchema);